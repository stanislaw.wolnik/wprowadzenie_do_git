EPICA - Consign to oblivion

ZWR-1:

	How can we let this happen and 
	Just keep our eyes closed 'till the end
 
	The only thing that counts is the prosperity of today 
	Most important to us is that our bills get paid 

	Our good intentions have always been delayed
 
REF-1:

	How can we let this happen and 
	Just keep our eyes closed 'till the end 
	When we will stand in front of heaven's gate 
	It will be too late!
 
ZWR-2:

	Try to unlearn all that you've learnt 
	Try to listen to your heart 
	No, we can't understand the universe 
	By just using our minds
 
BRIDGE:

	We are so afraid of all the things unknown 
	A must we appease is the lust to get laid 
	Nothing really matters, just devouring our prey
 
	Our good intentions have always been delayed so 
	Our generous acts have always come too late
 
REF-1: (powtarzamy)
 
ZWR-2: (Powtarzamy)
 
BRIDGE:

	We are so afraid of all the things unknown 
	We just flee into a dream that never comes true
 
	Low to the ground we feel safe 
	Low to the ground we feel brave
 
	Oblivisci tempta quod didicisti
 
REF-2:

	Open your eyes, we're not in paradise 
	How can't you see this stress is killing me? 
	Fulfil your dreams; life is not what is seems to be 
	We have captured time 
	So time made us all hostages without mercy 


BRIDGE:

	Seemingly generous fooling ourselves 
	Selfishly venomous time tells 

ZWR-3:

	Too much thinking goes at the cost of all our intuition 
	Our thoughts create reality 
	But we neglect to be! 
	So we're already slaves of our artificial world 
	We shouldn't try to control life 
	But listen to the laws of nature

REF-2: (Powtarzamy)

BRIDGE: 

	Low to the ground we feel safe 
	Low to the ground we feel brave 

	We all think we're generous 
	But we only fool ourselves 
	The only thing that matters is 

ZWR-4:

	Our way and our vision 
	Selfishly we're venomous 
	But you know the time tells us 
	There is more to life than our 
	Higher positions, race for perfection 
	Better, faster 
	We must return to the laws of the nature 
	Free ourselves from madness


